<?php

namespace App\Http\Controllers\Service;

use RouterOS\Client;
use RouterOS\Query;
use RouterOS;

class MicroticOSDataService
{

    public function addNewUserProfile($username, $password, $service, $profile_package, $comment)
    {

        // Initiate client with config object
        $client = new Client([
            'host' => '103.181.72.173',
            'user' => 'rashedul',
            'pass' => '0866',
            'port' => 8728,
            'ssh_timeout' => 60, // if not set then 30 seconds by default 
        ]);


        // $query =
        //     //  (new Query('ppp/user/profile/add'))
        //     (new Query('/ppp/secret/add'))
        //     ->equal('name', 'test-4')
        //     ->equal('comment', 'this is demo user')
        //     ->equal('service', 'pppoe')
        //     ->equal('password', 'test-4')
        //     ->equal('profile', '100 Mbps');


        $query = (new Query('/ppp/secret/add'))
            ->equal('name', $username,)
            ->equal('comment', $comment)
            ->equal('service', $service)
            ->equal('password', $password)
            ->equal('profile', $profile_package);

        // Create user on router
        $rs = $client->query($query)->read();

        return $rs;
        // Small timeout between requests is required
        sleep(.1);
        unset($client);
        // dd($rs);
    }

    public function getAvailabeUsersProfile()
    {

        // Initiate client with config object
        $client = new Client([
            'host' => '103.181.72.173',
            'user' => 'rashedul',
            'pass' => '0866',
            'port' => 8728,
            'ssh_timeout' => 60, // if not set then 30 seconds by default 
        ]);



        $query = new Query('/ppp/secret/print');
        // $query->where('name', 'Block');
        $secrets = $client->query($query)->read();
        dd($secrets);


        // $query = new Query('/ppp/active/remove');
        // // $query->where('name', 'Block');
        // $secrets = $client->query($query)->read();
        // dd($secrets);

        // $API->comm("/ppp/active/remove",
        // array(
        //     ".id" => $arrID[0][".id"],
        //     )
        // );
    }
}
