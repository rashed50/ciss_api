<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Service\CompanyInfoService;
use App\Http\Controllers\Service\CustomerInfoService;
use App\Http\Controllers\Service\MicroticOSDataService;
use App\Http\Controllers\Helpers\HelperService;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use Exception;


class ConnectionInfoController extends Controller
{
    public function addNewConnectionForm()
    {
        $customers = (new CustomerInfoService())->getCustomerList(1, 0);
        //dd($customers);
        $connections = (new CompanyInfoService())->getConnectionInformation(null);
        // dd($connections);
        return view('admin.connection.connection_add', compact('connections', 'customers'));
    }

    public function editConnectionInfoForm($id)
    {
        $connection = (new CompanyInfoService())->getConnectionInformation($id);
        return view('admin.connection.connection_edit', compact('connection'));
    }

    // Create User name, password for Profile in Router 
    public function insertConnectionInfoFormSubmit(Request $request)
    {

        try {


            $aCustomer = (new CustomerInfoService())->getCustomerInformation($request['customerAutoId']);
            $package = (new CompanyInfoService())->getPackageInformation($aCustomer->packageId);
            // dd($aCustomer, $package);
            // 
            if ($aCustomer->type_of_connectivity_id  == 1) {
                $service = 'pppoe';
            } else {
                $service = 'pppoe';
            }
            $packageProfile =  $package->bandwidth;
            // '100 Mbps';


            $result =  (new MicroticOSDataService())->addNewUserProfile($request['userId'],  $request['userPassword'], $service, $packageProfile, $request['description']);
            $resultAfter = $result['after'];
            // $message = $resultAfter['message'];
            // $ret = $resultAfter['ret'];

            //  dd($message, $ret);
            // dd($service, $result);
            // $isSuccess = false;
            // if (array_key_exists('message', $resultAfter)) {
            //     dd($resultAfter['message']);
            //     $isSuccess = false;
            // } else {

            //     dd($resultAfter['ret']);
            //     $isSuccess = true;
            // }

            $isError =  (new HelperService())->checkArrayKeyExistOrNot('message', $resultAfter);

            if ($isError) {
                // error occurred
                $notification = array(
                    'messege' => 'Did not Create User Profile!,  ' . $resultAfter['message'],
                    'alert-type' => 'error',
                );
                return redirect()->back()->with($notification);
            } else {

                $routerOSReponse = (new HelperService())->splitValueFromString('*', $resultAfter['ret']);
                if (count($routerOSReponse) >= 2 && $routerOSReponse[1] > 0) {

                    $connection = (new CompanyInfoService())
                        ->insertConnectionInformation(
                            $request['customerAutoId'],
                            $service,
                            $request['userId'],
                            $request['userPassword'],
                            $request['description'],
                        );
                    if ($connection > 0) {
                        (new CustomerInfoService())->customerConnectionStatusUpdate($request->customerAutoId, 2);
                        $notification = array(
                            'messege' => 'User Profile Created Successfully.   ' . $routerOSReponse[1],
                            // . $resultAfter['ret'],
                            'alert-type' => 'success',
                        );
                        return redirect()->route('connection_new_form')->with($notification);
                    }
                } else {
                    $notification = array(
                        'messege' => 'Did not Create User Profile!,  ' . $routerOSReponse[1],
                        'alert-type' => 'error',
                    );
                    return redirect()->back()->with($notification);
                }
            }
        } catch (Exception $ex) {
            $notification = array(
                'messege' => 'Please Try Again!',
                'alert-type' => 'error',
            );
            dd($ex);
            return redirect()->back()->with($notification);
        }
    }

    public function updateConnectionInfoFormSubmit(Request $request)
    {
        (new CompanyInfoService())->updateConnectionInformation(
            $request->connectionId,
            $request['customerAutoId'],
            $request['ipAddress'],
            $request['userId'],
            $request['userPassword'],
            $request['description'],
        );

        (new CustomerInfoService())->customerConnectionStatusUpdate($request->customerAutoId, 2);

        $notification = array(
            'messege' => 'Connection Update Success!',
            'alert-type' => 'success',
        );
        return redirect()->route('connection_new_form')->with($notification);
    }

    public function deleteConnectionInfoFormSubmit(Request $request)
    {
        $id = $request['connection_info_id'];

        //  dd($id);
        $connections = (new CompanyInfoService())->getConnectionInformation($id);
        (new CustomerInfoService())->customerConnectionStatusUpdate($connections->customerAutoId, 1);
        (new CustomerInfoService())->deleteCustomerConnectionInformation($id);

        $notification = array(
            'messege' => 'Connection Delete Success!',
            'alert-type' => 'success',
        );
        return redirect()->route('connection_new_form')->with($notification);
    }

    public function updateCustomerConnectionStatus(Request $request)
    {

        // (new CustomerInfoService())->customerConnectionStatusUpdate(
        //     $request->connectionStatusId,
        // );
        //   (new CustomerInfoService())->customerConnectionStatusUpdate($request->customerAutoId, 2);


        $notification = array(
            'messege' => 'Connection Status Update Success!',
            'alert-type' => 'success',
        );
        return redirect()->route('connection_new_form')->with($notification);
    }



    //  ===================================================================
    //  ================= Connection Info API SECTION =====================
    //  ===================================================================





    public function saveConnectionInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ipAddress' => 'required',
            'userId' => 'required',
            'userPassword' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => 'false', 'status_code' => '401', 'error' => 'error', 'message' => $validator->errors()]);
        }

        try {
            $customerAutoId = $request->input('customerAutoId');
            $ipAddress = $request->input('ipAddress');
            $userId = $request->input('userId');
            $userPassword = $request->input('userPassword');
            $description = $request->input('description');

            $connection = (new CompanyInfoService())->insertConnectionInformation($customerAutoId, $ipAddress, $userId, $userPassword, $description);

            return response()->json(['success' => 'true', 'status_code' => '200', 'data' => $connection]);
        } catch (Exception $ex) {
            return response()->json(['success' => 'false', 'status_code' => '500', 'message' => $ex->getMessage(), 'error' => 'error']);
        }
    }


    public function getConnectionInfo($id = null)
    {

        try {

            $connections = (new CompanyInfoService())->getConnectionInformation($id);
            return response()->json(['success' => 'true', 'status_code' => '200', 'data' => $connections]);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'success' => 'false', 'status_code' => '404',
                'error' => 'Invalid:Model Not Found'
            ]);
        } catch (Exception $ex) {
            return response()->json(['success' => 'false', 'status_code' => '500', 'error' => $ex->getMessage()]);
        }
    }


    // public function deleteCustomerConnectionInformation($connectionId){

    //    $connectInfo = $this->getConnectionInfo();

    //  }









}
